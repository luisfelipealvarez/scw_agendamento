"use strict";
var request = require('request');
var htmlParser = require('html-parser');
var _ = require('lodash');
var tweet = require('./tweet.js');

require('dotenv').config()

const is_development = process.env.DEVELOPMENT;

var tellTheWorld = function(local, dates){
	
	if(dates.length > 0 && local.should_tweet){
		dates = dates.join(', ');
		var text = "Em " + local.name + " ha vagas para agendamento disponivels " + dates;
		if(is_development){
			console.log(text);
		}
		else{
			tweet.tweet(text);
		}
	}
};

var locals = [{code: "DELEMIG/AC;0035;3034040018",name: "DELEMIG/AC - RIO BRANCO", should_tweet: false},
				{code: "DPF/CZS/AC;0876;3034060062",name: "DPF/CZS/AC - CRUZEIRO DO SUL", should_tweet: false},
				{code: "DPF/EPA/AC;0302;3034060051",name: "DPF/EPA/AC - EPITACIOLANDIA", should_tweet: false},
				{code: "DELEMIG/AL;0043;3034042614",name: "DELEMIG/AL - MACEIO", should_tweet: false},
				{code: "DELEMIG/AM;0060;3034040816",name: "DELEMIG/AM - MANAUS", should_tweet: false},
				{code: "DPF/TBA/AM;0329;3034060868",name: "DPF/TBA/AM - TABATINGA", should_tweet: false},
				{code: "DELEMIG/AP;0051;3034050412",name: "DELEMIG/AP - MACAPA", should_tweet: false},
				{code: "DPF/OPE/AP;0310;3034060450",name: "DPF/OPE/AP - OIAPOQUE", should_tweet: false},
				{code: "DELEMIG/BA;0078;3034042819",name: "DELEMIG/BA - SALVADOR", should_tweet: false},
				{code: "DPF/ILS/BA;0345;3034052857",name: "DPF/ILS/BA - ILHEUS", should_tweet: false},
				{code: "DPF/JZO/BA;0337;3034062887",name: "DPF/JZO/BA - JUAZEIRO", should_tweet: false},
				{code: "DPF/PSO/BA;0965;3034062898",name: "DPF/PSO/BA - PORTO SEGURO", should_tweet: false},
				{code: "DPF/VDC/BA;1376;3034052870",name: "DPF/VDC/BA - VITORIA DA CONQUISTA", should_tweet: false},
				{code: "DELEMIG/CE;0086;3034043211",name: "DELEMIG/CE - FORTALEZA", should_tweet: false},
				{code: "DPF/JNE/CE;1104;3034069280",name: "DPF/JNE/CE - JUAZEIRO DO NORTE", should_tweet: false},
				{code: "AEROPORTOJK;0094;3034048116",name: "AEROPORTOJK - BRASILIA", should_tweet: false},
				{code: "RIACHOMALL;0094;3034000600",name: "RIACHOMALL - BRASILIA", should_tweet: false},
				{code: "DELEMIG/ES;0108;3034044617",name: "DELEMIG/ES - VITORIA", should_tweet: false},
				{code: "DPF/CIT/ES;0884;3034070009",name: "DPF/CIT/ES - CACHOEIRO DO ITAPEMIRIM", should_tweet: false},
				{code: "DPF/SMT/ES;1112;3034069272",name: "DPF/SMT/ES - SAO MATEUS", should_tweet: false},
				{code: "DELEMIG/GO;0116;3034049015",name: "DELEMIG/GO - GOIANIA", should_tweet: false},
				{code: "DPF/ANS/GO;0353;3034069032",name: "DPF/ANS/GO - ANAPOLIS", should_tweet: false},
				{code: "DPF/JTI/GO;0892;3034069051",name: "DPF/JTI/GO - JATAI", should_tweet: false},
				{code: "DELEMIG/MA;0124;3034043416",name: "DELEMIG/MA - SAO LUIS", should_tweet: false},
				{code: "DEPOM;0124;3034114445",name: "DEPOM", should_tweet: false},
				{code: "DPF/CXA/MA;1180;3034063460",name: "DPF/CXA/MA - CAXIAS", should_tweet: false},
				{code: "DPF/ITZ/MA;0361;3034063450",name: "DPF/ITZ/MA - IMPERATRIZ", should_tweet: false},
				{code: "DELEMIG/MG;0159;3034044919",name: "DELEMIG/MG - BELO HORIZONTE", should_tweet: false},
				{code: "DPF/DVS/MG;1384;3034069480",name: "DPF/DVS/MG - DIVINOPOLIS", should_tweet: false},
				{code: "DPF/GVS/MG;0442;3034064952",name: "DPF/GVS/MG - GOVERNADOR VALADARES", should_tweet: false},
				{code: "DPF/JFA/MG;0450;3034064928",name: "DPF/JFA/MG - JUIZ DE FORA", should_tweet: false},
				{code: "DPF/MOC/MG;0981;3034064971",name: "DPF/MOC/MG - MONTES CLAROS", should_tweet: false},
				{code: "DPF/UDI/MG;0868;3034064939",name: "DPF/UDI/MG - UBERLANDIA", should_tweet: false},
				{code: "DPF/URA/MG;0469;3034064944",name: "DPF/URA/MG - UBERABA", should_tweet: false},
				{code: "DPF/VAG/MG;0973;3034064963",name: "DPF/VAG/MG - VARGINHA", should_tweet: false},
				{code: "DELEMIG/MS;0400;3034048213",name: "DELEMIG/MS - CAMPO GRANDE", should_tweet: false},
				{code: "DPF/CRA/MS;0400;3034068257",name: "DPF/CRA/MS - CORUMBA", should_tweet: false},
				{code: "DPF/DRS/MS;0418;3034068311",name: "DPF/DRS/MS - DOURADOS", should_tweet: false},
				{code: "DPF/NVI/MS;0426;3034068320",name: "DPF/NVI/MS - NAVIRAI", should_tweet: false},
				{code: "DPF/PPA/MS;0396;3034068265",name: "DPF/PPA/MS - PONTA PORA", should_tweet: false},
				{code: "DPF/TLS/MS;0434;3034068303",name: "DPF/TLS/MS - TRES LAGOAS", should_tweet: false},
				{code: "DELEMIG/MT;0132;3034049210",name: "DELEMIG/MT - CUIABA", should_tweet: false},
				{code: "DPF/BRG/MT;0370;3034069245",name: "DPF/BRG/MT - BARRA DO GARCA", should_tweet: false},
				{code: "DPF/CAE/MT;0388;3034069237",name: "DPF/CAE/MT - CACERES", should_tweet: false},
				{code: "DPF/ROO/MT;0906;3034069221",name: "DPF/ROO/MT - RONDONOPOLIS", should_tweet: false},
				{code: "DPF/SIC/MT;1350;3034069359",name: "DPF/SIC/MT - SINOP", should_tweet: false},
				{code: "DELEMIG/PA;0167;3034041510",name: "DELEMIG/PA - BELEM", should_tweet: false},
				{code: "DPF/ATM/PA;1171;3034006934",name: "DPF/ATM/PA - ALTAMIRA", should_tweet: false},
				{code: "DPF/MBA/PA;0477;3034061570",name: "DPF/MBA/PA - MARABA", should_tweet: false},
				{code: "DPF/RDO/PA;1120;3034069329",name: "DPF/RDO/PA - REDENCAO", should_tweet: false},
				{code: "DPF/SNM/PA;0493;3034061554",name: "DPF/SNM/PA - SANTAREM", should_tweet: false},
				{code: "DELEMIG/PB;0175;3034043610",name: "DELEMIG/PB - JOAO PESSOA", should_tweet: false},
				{code: "DPF/CGE/PB;0507;3034063646",name: "DPF/CGE/PB - CAMPINA GRANDE", should_tweet: false},
				{code: "DPF/PAT/PB;1333;3034006933",name: "DPF/PAT/PB - PATOS", should_tweet: false},
				{code: "DELEMIG/PE;0191;3034043815",name: "DELEMIG/PE - RECIFE", should_tweet: false},
				{code: "DPF/CRU/PE;1139;3034069264",name: "DPF/CRU/PE - CARUARU", should_tweet: false},
				{code: "DPF/SGO/PE;0990;3034069027",name: "DPF/SGO/PE - SALGUEIRO", should_tweet: false},
				{code: "DELEMIG/PI;0205;3034044013",name: "DELEMIG/PI - TERESINA", should_tweet: false},
				{code: "DPF/PHB/PI;1317;3034043851",name: "DPF/PHB/PI - PARNAIBA", should_tweet: false},
				{code: "DELEMIG/PR;0183;3034045710",name: "DELEMIG/PR - DELEMIG/PR", should_tweet: false},
				{code: "DPF/CAC/PR;1210;3034065811",name: "DPF/CAC/PR - CASCAVEL", should_tweet: false},
				{code: "DPF/FIG/PR;0515;3034055740",name: "DPF/FIG/PR - FOZ DO IGUACU", should_tweet: false},
				{code: "DPF/GPB/PR;0914;3034065846",name: "DPF/GPB/PR - GUARAPUAVA", should_tweet: false},
				{code: "DPF/GRA/PR;0531;3034065835",name: "DPF/GRA/PR - GUAIRA", should_tweet: false},
				{code: "DPF/LDA/PR;0523;3034055821",name: "DPF/LDA/PR - LONDRINA", should_tweet: false},
				{code: "DPF/MGA/PR;0540;3034069253",name: "DPF/MGA/PR - MARINGA", should_tweet: false},
				{code: "DPF/PGZ/PR;1392;3034069499",name: "DPF/PGZ/PR - PONTA GROSSA", should_tweet: false},
				{code: "DPF/PNG/PR;0558;3034065800",name: "DPF/PNG/PR - PARANAGUA", should_tweet: false},
				{code: "DELEMIG/RJ;0213;3034045117",name: "DELEMIG/RJ - RIO DE JANEIRO", should_tweet: true},
				{code: "DPF/ARS/RJ;1228;3034069477",name: "DPF/ARS/RJ - ANGRA DOS REIS", should_tweet: false},
				{code: "DPF/GOY/RJ;1007;3034065323",name: "DPF/GOY/RJ - CAMPOS DOS GOYTACAZES", should_tweet: false},
				{code: "DPF/MCE/RJ;0582;3034065304",name: "DPF/MCE/RJ - MACAE", should_tweet: false},
				{code: "DPF/NIG/RJ;0574;3034055279",name: "DPF/NIG/RJ - NOVA IGUACU", should_tweet: false},
				{code: "DPF/NRI/RJ;0566;3034055198",name: "DPF/NRI/RJ - NITEROI", should_tweet: false},
				{code: "DPF/VRA/RJ;0922;3034065854",name: "DPF/VRA/RJ - VOLTA REDONDA", should_tweet: false},
				{code: "DELEMIG/RN;0221;3034044218",name: "DELEMIG/RN - NATAL", should_tweet: false},
				{code: "DPF/MOS/RN;1155;3034069299",name: "DPF/MOS/RN - MOSSORO", should_tweet: false},
				{code: "DELEMIG/RO;0248;3034041812",name: "DELEMIG/RO - PORTO VELHO", should_tweet: false},
				{code: "DPF/GMI/RO;0710;3034061821",name: "DPF/GMI/RO - GUAJARA MIRIM", should_tweet: false},
				{code: "DPF/JPR/RO;1015;3034061883",name: "DPF/JPR/RO - JI-PARANA", should_tweet: false},
				{code: "DPF/VLA/RO;0728;3034061872",name: "DPF/VLA/RO - VILHENA", should_tweet: false},
				{code: "DELEMIG/RR;0256;3034052210",name: "DELEMIG/RR - BOA VISTA", should_tweet: false},
				{code: "DPF/PAC/RR;0256;3034062281",name: "DPF/PAC/RR - PACARAIMA", should_tweet: false},
				{code: "DELEMIG/RS;0230;3034047110",name: "DELEMIG/RS - PORTO ALEGRE", should_tweet: false},
				{code: "DPF/BGE/RS;0639;3034057158",name: "DPF/BGE/RS - BAGE", should_tweet: false},
				{code: "DPF/CHI/RS;0647;3034067420",name: "DPF/CHI/RS - CHUI", should_tweet: false},
				{code: "DPF/CXS/RS;0655;3034067431",name: "DPF/CXS/RS - CAXIAS DO SUL", should_tweet: false},
				{code: "DPF/JGO/RS;0671;3034067218",name: "DPF/JGO/RS - JAGUARAO", should_tweet: false},
				{code: "DPF/LIV/RS;0698;3034067307",name: "DPF/LIV/RS - SANTANA DO LIVRAMENTO", should_tweet: false},
				{code: "DPF/PFO/RS;0612;3034067440",name: "DPF/PFO/RS - PASSO FUNDO", should_tweet: false},
				{code: "DPF/PTS/RS;1074;3034067458",name: "DPF/PTS/RS - PELOTAS", should_tweet: false},
				{code: "DPF/RGE/RS;0590;3034057271",name: "DPF/RGE/RS - RIO GRANDE", should_tweet: false},
				{code: "DPF/SAG/RS;0612;3034057387",name: "DPF/SAG/RS - SANTO ANGELO", should_tweet: false},
				{code: "DPF/SBA/RS;0701;3034067315",name: "DPF/SBA/RS - SAO BORJA", should_tweet: false},
				{code: "DPF/SCS/RS;0230;3034067466",name: "DPF/SCS/RS - SANTA CRUZ DO SUL", should_tweet: false},
				{code: "DPF/SMA/RS;0604;3034067374",name: "DPF/SMA/RS - SANTA MARIA", should_tweet: false},
				{code: "DPF/UGA/RS;0620;3034057336",name: "DPF/UGA/RS - URUGUAIANA", should_tweet: false},
				{code: "DELEMIG/SC;0264;3034046512",name: "DELEMIG/SC - FLORIANOPOLIS", should_tweet: false},
				{code: "DPF/CCM/SC;1368;3030066580",name: "DPF/CCM/SC - CRICIUMA", should_tweet: false},
				{code: "DPF/DCQ/SC;0736;3034066521",name: "DPF/DCQ/SC - DIONISIO CERQUEIRA", should_tweet: false},
				{code: "DPF/IJI/SC;0744;3034066548",name: "DPF/IJI/SC - ITAJAI", should_tweet: false},
				{code: "DPF/JVE/SC;0752;3034066556",name: "DPF/JVE/SC - JOINVILLE", should_tweet: false},
				{code: "DPF/LGE/SC;1201;3034066575",name: "DPF/LGE/SC - LAGES", should_tweet: false},
				{code: "DPF/XAP/SC;0930;3034066567",name: "DPF/XAP/SC - CHAPECO", should_tweet: false},
				{code: "DELEMIG/SE;0280;3034044412",name: "DELEMIG/SE - ARACAJU", should_tweet: false},
				{code: "DELEMIG/SP;0272;3034045532",name: "DELEMIG/SP - SAO PAULO", should_tweet: false},
				{code: "DPF/AQA/SP;1023;3034065609",name: "DPF/AQA/SP - ARARAQUARA", should_tweet: false},
				{code: "DPF/ARU/SP;0949;3034065595",name: "DPF/ARU/SP - ARACATUBA", should_tweet: false},
				{code: "DPF/BRU/SP;0787;3034065444",name: "DPF/BRU/SP - BAURU", should_tweet: false},
				{code: "DPF/CAS/SP;0779;3034065495",name: "DPF/CAS/SP - CAMPINAS", should_tweet: false},
				{code: "DPF/CZO/SP;1163;3034069302",name: "DPF/CZO/SP - CRUZEIRO", should_tweet: false},
				{code: "DPF/JLS/SP;1082;3034065455",name: "DPF/JLS/SP - JALES", should_tweet: false},
				{code: "DPF/MII/SP;1031;3034065579",name: "DPF/MII/SP - MARILIA", should_tweet: false},
				{code: "DPF/PCA/SP;1090;3034065668",name: "DPF/PCA/SP - PIRACICABA", should_tweet: false},
				{code: "DPF/PDE/SP;0795;3034065550",name: "DPF/PDE/SP - PRESIDENTE PRUDENTE", should_tweet: false},
				{code: "DPF/RPO/SP;0809;3034065568",name: "DPF/RPO/SP - RIBEIRAO PRETO", should_tweet: false},
				{code: "DPF/SJE/SP;0817;3034065541",name: "DPF/SJE/SP - SAO JOSE DO RIO PRETO", should_tweet: false},
				{code: "DPF/SJK/SP;0957;3034065625",name: "DPF/SJK/SP - SAO JOSE DOS CAMPOS", should_tweet: false},
				{code: "DPF/SOD/SP;1040;3034065617",name: "DPF/SOD/SP - SOROCABA", should_tweet: false},
				{code: "DPF/SSB/SP;0825;3034065509",name: "DPF/SSB/SP - SAO SEBASTIAO", should_tweet: false},
				{code: "DPF/STS/SP;0760;3034055457",name: "DPF/STS/SP - SANTOS", should_tweet: false},
				{code: "DELEMIG/TO;0299;3034049021",name: "DELEMIG/TO - PALMAS", should_tweet: false},
				{code: "DPF/AGA/TO;0833;3034069040",name: "DPF/AGA/TO - ARAGUAINA", should_tweet: false}]


var url = 'https://servicos.dpf.gov.br/sincreWeb/dataAtendimento?randon='

var determineUrl = function(local){
	var rand = _.random(0, 1, true);
	// This is very funny, the random doesn't seem to be doing anything, I believe it must have been some kind of 
	// csrf thing.
	return url + String(rand) + "&orgao=" +local.code;
}

var dateParser = function(value) {
	    	value = value.trim();
	    	console.log(value);
	    	if(value.length > 0){
	    		dates.push(value);
	    	}
		};

var parseBodyAndGetDates = function(str){
	var dates = [];
	var year = (new Date()).getFullYear();
	htmlParser.parse(str, {
		attribute: function(name, value) {
			value = value.trim();
			if(name == "value" && value.length > 0){
				value = value.replace("/" + year, "");
				dates.push(value);

			}
		}});
	return dates;
};


var dates_getter = function(local){
		
	var url_ = determineUrl(local);
	request(url_, function (error, response, body) {
		var dates = parseBodyAndGetDates(body);
		tellTheWorld(local, dates);
	});

}


var getCodes =  function(){
	var result = [];

	_.each(locals, function(local){
		dates_getter(local);
	})
}
getCodes();