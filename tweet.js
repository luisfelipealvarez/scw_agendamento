"use strict";
var Twitter = require('twitter');
require('dotenv').config()
 
var client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});

var tweet = function(text){
	client.post('statuses/update', {status: text},  function(error, tweet, response) {
	  if(error){
	  	console.log(error);
	  };
	});
}

module.exports = {tweet: tweet};